# Flask Images Specifications REST API 
## _REST API for getting and storing image dimensions,extension and sha1 hash_

## Summary
This API can help you get information for a certain image from URL or by adding folder with image files.
In the case of URL no files are stored only info, because of Copyright rules. 
Every sensitive information is stored and passed withthe help of .env file.

## Features
- Utilizing PostgreSQL as a database and Python Flask for manipulating it.
- Different endpoints for checking and storing image props from URL or folder with image files.
- Can function as a Docker container or standalone API.

## Tech

Some of the technologies used:

- [Python](https://www.python.org/) - interpreted high-level general-purpose programming language.
- [Flask](https://flask.palletsprojects.com/en/2.0.x/) - micro web framework written in Python.
- [Docker](https://www.docker.com/) - awesome containerization.
- [Docker-Compose](https://docs.docker.com/compose/) - Fast and easy to use docker helper.
- [HTTPIE](https://httpie.io/) - awesome cli replacement for Postman
- [Postman](https://www.postman.com/) - the standard API testing tool

## Requirements
In case of using as docker container:
-  Docker 
-  Docker-compose
-  httpie
-  Postman

In case of using it as is:
- Python
- Python-pip
- Python-venv
- Existing PostgreSQL server
- httpie
- Postman

## Project structure

```
image_specs
├── config.py
├── db.py
├── docker-compose.yaml
├── Dockerfile
├── main.py
├── managers
│   ├── images.py
│   └── __init__.py
├── models
│   ├── images.py
│   └── __init__.py
├── README.md
├── requirements.txt
├── resources
│   ├── images.py
│   ├── __init__.py
│   └── routes.py
├── schemas
│   ├── __init__.py
│   ├── request
│   │   └── images.py
│   └── response
│       └── images.py
└── utils
    ├── decorators.py
    ├── generators.py
    ├── helpers.py
    └── __init__.py

```
- `managers/` - files in this directory are responsible for communicating with database
- `models/` - files that keep the definition of database tables and relations
- `resources/` - files defining the behavior of the GET,POST,PUT,DELETE requests
- `schemas/` - files defining validation methods for the input
- `utils/` - files that keep all the helper funtions that can be used everywhere in the app

## Installation
You can use and manage the API like a docker container or like a standalone web application.
##### Steps to install the API:
1. Clone the repository:
```
git clone https://gitlab.com/antonpetrov145/image_specs
```
2. Change into directory: 
```
cd image_specs/
```
Depending on the method you chose to run the API follow instructions below:
#### Docker
3. Create .env file with this structure:
```
SECRET_KEY=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DB=
FILES_FOLDER=
```
`NOTE!` - these values are written by you and API will use them
- `SECRET_KEY` - random string for the app
- `POSTGRES_USER` - database user that will query the database
- `POSTGRES_PASSWORD` - password for the user
- `POSTGRES_DB` - name of the database
- `FILES_FOLDER` - folder with images (in case you have one) to feed the API
4. Run and build the container with docker compose:
```
docker-compose up -d --build
```
#### Standalone
3. Create .env file with this structure:
```
SECRET_KEY=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DB=
POSTGRES_HOST=
POSTGRES_PORT=
FILES_FOLDER=
```
`NOTE!` - these values are written by you and API will use them
- `SECRET_KEY` - random string for the app
- `POSTGRES_USER` - database user that will query the database
- `POSTGRES_PASSWORD` - password for the user
- `POSTGRES_DB` - name of the database
- `POSTGRES_HOST` - the IP address of your PostgreSQL server
- `POSTGRES_PORT` - the port that accepts connections to the PostgreSQL server
- `FILES_FOLDER` - folder with images (in case you have one) to feed the API
4. Create virtual environment and start using it:
```
python -m venv venv
venv/bin/activate
```
5. Install the required packages from requirements.txt file:
```
pip install -r requirements.txt
```
6. Run the API:
```
python main.py
```

## Usage
The API has two endpoints:
- `/images` - accepts `GET` and `POST` requests
- `/image_files` - accepts `POST` request

#### Docker
The IP address of the container that accepts requests is `localhost` and the port is `81`. 
In the container is installed `pgadmin` tool that is accessible in browser on port `5001`.
To enter `pgadmin` the E-mail is `POSTGRES_USER@admin.com`, `POSTGRES_USER` you set earlier in the .env file.
Password is the `POSTGRES_PASSWORD` from the same file.
The name of the database to look for in `pgadmin` is set with `POSTGRES_DB` in the .env file.

You can use whichever tool you know. Here is example request and response with HTTPie:
The GET request on `/images` route gets you all of the saved image dimensions in the database (if any):
Example equest:
```
http GET http://localhost:81/images
```
Example response:
```
HTTP/1.1 200 OK
Connection: close
Content-Length: 106387
Content-Type: application/json
Date: Sun, 27 Feb 2022 16:01:21 GMT
Server: gunicorn

[
    {
        "dimensions": "1200x1500",
        "extension": "jpg",
        "sha1_hash": "f9be7102761d47f8b527e1a01f008f9dc1829ce8",
        "url": "https://images.pexels.com/photos/11284548/pexels-photo-11284548.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
    }
]
```
The POST requests on `/images` has only one parameter to function - `url` which is the URL of the image. This request fetches the image and strips the data from it, after that the data is saved in the database:

Example request:
```
http POST http://localhost:81/images url=https://images.pexels.com/photos/3831760/pexels-photo-3831760.jpeg\?auto\=compress\&cs\=tinysrgb\&dpr\=2\&h\=750\&w\=1260
```
Example response:
```
HTTP/1.1 201 CREATED
Connection: close
Content-Length: 245
Content-Type: application/json
Date: Sun, 27 Feb 2022 16:21:52 GMT
Server: gunicorn

{
    "dimensions": "1000x1500",
    "extension": "jpg",
    "sha1_hash": "9e1f0c065886f0dafbbcb67f35f80ca23d068d03",
    "url": "https://images.pexels.com/photos/3831760/pexels-photo-3831760.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
}
```
The POST requests on `/image_files` has only one parameter to function - `url` which has to be some random string with lenght at least 10 characters. 
This route uses the `FILES_FOLDER` you set in the .env file and fetches all of the image files and strips the data from them.
#### Standalone
The IP address that accepts requests is `127.0.0.1` on port `5000`.
The requests and the responses are the same as in the dockerized version.