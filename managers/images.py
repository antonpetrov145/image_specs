from db import db
from models.images import ImagesModel


class ImagesManager:
    @staticmethod
    def create(img_specs, url):
        img_specs["url"] = url
        img = ImagesModel(**img_specs)
        db.session.add(img)
        db.session.flush()
        return img


class ImageFilesManager:
    @staticmethod
    def create(img_specs):
        img = ImagesModel(**img_specs)
        db.session.add(img)
        db.session.flush()
        return img
