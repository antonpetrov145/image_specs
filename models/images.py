from db import db


class ImagesModel(db.Model):
    __tablename__ = "image_specs"

    pk = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String, nullable=False)
    dimensions = db.Column(db.String, nullable=False)
    extension = db.Column(db.String, nullable=False)
    sha1_hash = db.Column(db.String, nullable=False)
