from functools import wraps

import requests
import validators
from flask import request
from werkzeug.exceptions import BadRequest


def validate_picture(f):
    """
    Decorator that validates if the provided URL has image in it
    with the help of the 'content-type' header

    Args:
        f (function): function to be decorated

    Raises:
        BadRequest: if the URL is not correct

    Returns:
        f: decorated function
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        data = request.get_json()
        pic_headers = (
            "image/png",
            "image/jpeg",
            "image/jpg",
            "image/svg+xml",
            "image/x-icon",
            "image/bmp",
            "image/gif",
        )
        res = requests.head(data["url"])
        try:
            if res.headers["content-type"] in pic_headers:
                return f(*args, **kwargs)
            else:
                raise BadRequest(f"Invalid image URL")
        except Exception:
            raise BadRequest(f"Invalid or incomplete image URL")

    return decorated_function


def validate_schema(schema_name):
    """Validate against a marshmallow schema

    Args:
        schema_name (Schema): marshmallow schema
    """

    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            schema = schema_name()
            errors = schema.validate(request.get_json())
            if errors:
                raise BadRequest(f"Invalid fields {errors}")
            return f(*args, **kwargs)

        return decorated_function

    return decorator


def validate_url(f):
    """
    Validates the URL.

    Args:
        f (function): function to be decorated

    Raises:
        BadRequest: in case the URL is invalid or incomplete

    Returns:
        f: decorated function
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        data = request.get_json()
        url = validators.url(data["url"])
        try:
            if url:
                return f(*args, **kwargs)
            elif not url:
                raise BadRequest(f"Invalid or incomplete URL!")
        except Exception as e:
            raise BadRequest(f"Invalid or incomplete URL")

    return decorated_function
