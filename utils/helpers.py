import hashlib
import io
import mimetypes

import requests
from PIL import Image, ImageFile


def get_url():
    pass


def get_response_object(url):
    hd = {"Range": "bytes=0-50000"}
    s = requests.get(url, stream=True, headers=hd)
    return s


def get_image_specs(res):
    """
    Function to get all of the required data from
    URL with image.

    Args:
        res: response object from request to URL

    Returns:
        Dictionary: python dictionary with the stripped data
    """
    p = ImageFile.Parser()
    p.feed(res.content)

    content_type = res.headers["content-type"]
    extension = mimetypes.guess_extension(content_type)

    ImageFile.LOAD_TRUNCATED_IMAGES = True
    filename = io.BytesIO(res.content)
    sha1_hash = hashlib.sha1(Image.open(filename).tobytes())
    return {
        "dimensions": f"{p.image.size[0]}x{p.image.size[1]}",
        "extension": extension.lstrip("."),
        "sha1_hash": sha1_hash.hexdigest(),
    }
