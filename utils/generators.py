import glob
import hashlib
import ntpath
from PIL import Image
from werkzeug.exceptions import BadRequest


def generate_image_specs(dir):
    formats = ["jpg", "jpeg", "png", "gif"]
    files = []
    [files.extend(glob.glob(str(dir) + "*." + str(f))) for f in formats]
    if files:
        for f in files:
            filename = ntpath.basename(f)
            i = Image.open(f)
            extension = f.split(".")[-1]
            sha1_hash = hashlib.sha1(i.tobytes())
            yield {
                "url": filename,
                "dimensions": f"{i.size[0]}x{i.size[1]}",
                "extension": extension.lstrip("."),
                "sha1_hash": sha1_hash.hexdigest(),
            }
    elif not files:
        raise BadRequest("Folder empty or not containing images!")
