from resources.images import Images, ImageFiles

routes = (
    (Images, "/images"),
    (ImageFiles, "/image_files"),
)
