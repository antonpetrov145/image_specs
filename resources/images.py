from flask import request
from flask_restful import Resource
from managers.images import ImagesManager, ImageFilesManager
from models.images import ImagesModel
from schemas.request.images import ImageRequestSchema
from schemas.response.images import ImageResponseSchema
from utils.decorators import validate_picture, validate_schema, validate_url
from utils.generators import generate_image_specs
from utils.helpers import get_image_specs, get_response_object


class Images(Resource):
    def get(self):
        images = ImagesModel.query.all()
        schema = ImageResponseSchema()
        return schema.dump(images, many=True), 200

    @validate_schema(ImageRequestSchema)
    @validate_url
    @validate_picture
    def post(self):
        data = request.get_json()
        url = data["url"]
        res = get_response_object(url)
        img = get_image_specs(res)
        ImagesManager.create(img, url)
        return img, 201


class ImageFiles(Resource):
    @validate_schema(ImageRequestSchema)
    def post(self):
        data = request.get_json()
        images = generate_image_specs("/assets/")
        for i in images:
            ImageFilesManager.create(i)
        return {"message": f"done adding"}, 201
