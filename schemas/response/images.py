from marshmallow import Schema, fields


class ImageResponseSchema(Schema):
    dimensions = fields.String(required=True)
    extension = fields.String(required=True)
    sha1_hash = fields.String(required=True)
    url = fields.String(required=True)
