from marshmallow import Schema, fields, validate


class ImageRequestSchema(Schema):
    url = fields.String(required=True, validate=validate.Length(min=10))
